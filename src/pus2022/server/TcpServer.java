package pus2022.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Properties;

/**
 *
 * @author jaroc
 */
public class TcpServer {
    
    public static int tcpPort = 5555;
    
    private final ServerSocket serverSocket;
    
    public static HashSet<ClientThread> clientsPool = new HashSet<>();
    
    public TcpServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }
    
    public void run() {
        for(;;) {
            try {
                Socket clientSocket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(clientSocket);
                clientsPool.add(clientThread);
                new Thread(clientThread).start();
            } catch(IOException ex) {
                System.err.println("Error (" + ex.getMessage() + ") during accepting tcpServer.serverSocket");
            }
        }
    }
    
    public static void main(String[] args) {
        try {
            String propFileName = "server.properties";
            Properties props = new Properties();
            props.load(new FileInputStream(propFileName));
            TcpServer.tcpPort = Integer.parseInt(props.getProperty("port"));
        } catch (IOException e) {
            System.err.println("Error reading configuration: " + e.getMessage());
            System.exit(1);
        }

        System.out.println("TCP Server 0.0.1");
        try {
            TcpServer tcpServer = new TcpServer(TcpServer.tcpPort);
            tcpServer.run();
        } catch(IOException ex) {
            System.err.println("Cannot create TcpServer instance (cause: " + ex.getMessage() + "), aborting...");
        }
    }
}
